# F-UJI test

Repository to test scoring in https://www.f-uji.net/

## Building the project

We strongly recommend to use [Python virtual environment](https://docs.python.org/3/tutorial/venv.html). We use `python3` in the command due to Debian. It may be `python` on your system.
```bash
python3 -m venv env
source env/bin/activate
pip install -U pip
pip install -r requirements.txt
```
To deactivate the virtual environment, execute `deactivate` command.

To remove the environment, simply delete it with `rm -rf env`.
