from jinja2 import Environment, FileSystemLoader, select_autoescape
from pathlib import Path
import shutil
import json

# Create the output directory.
output_path = Path('output')
output_path.mkdir(exist_ok = True)

# Copy static content to output.
shutil.copy(Path(__file__).parent / 'metadata.json', output_path)

# Create Jinja environment
templates_path = Path(__file__).parent / 'templates'
env = Environment(
    loader=FileSystemLoader(templates_path),
    autoescape=select_autoescape()
)

# Initialize template context.
template_vars = {}

# Convert README contents to HTML and add to template context.
with open('metadata.json', 'r') as jsonld_file:
    metadata_json = json.load(jsonld_file)
    # metadata_content = jsonld_file.read()
    # metadata_content = json.dumps(metadata_json, separators=(',', ':'))
template_vars.update({'metadata_content': metadata_json})

# Render index page.
index_page_path = output_path / 'index.html'
template = env.get_template('index.html')
with index_page_path.open('w') as index_page_file:
    index_page_file.write(
        template.render(**template_vars)
    )
